﻿==============================================================
ソフト名：Lesserkai（れさ改）
バージョン：1.4.0
動作環境：Windows Vista/7/8/10
種別：フリーソフト
サポート：http://www.geocities.jp/shogidokoro/
==============================================================

Lesserkaiについて

Lesserkaiは、USI(Universal Shogi Intereface)プロトコルに対応した
将棋エンジンのサンプルです。将棋エンジン作者の参考になるよう、
ソースを公開します。
USIプロトコルに関する解説は、将棋所のページをご覧下さい。

将棋所の作者による解説
http://www.geocities.jp/shogidokoro/usi.html

USIの原案（リンク切れ）
http://www.glaurungchess.com/shogi/usi.html

==============================================================

仕様許諾条件

Lesserkaiは、池泰弘様作成の将棋ソフト「れさぴょん」を元に作成
したものです。コマンド入出力部分は変更していますが、思考ルーチンは
「れさぴょん」ほとんどそのままですので、仕様許諾条件としては
「れさぴょん」に準ずるものとします。詳しくは「れさぴょん」の
ページをご覧下さい。

http://usapyon.game.coocan.jp/lesserpyon/

==============================================================

コンパイル環境

Lesserkaiをコンパイルするためには、Microsoft Visual C++ 2010以降が
必要になります。以下のページからフリー版をダウンロードできます。

http://www.visualstudio.com/downloads/download-visual-studio-vs

==============================================================

更新履歴

2016/04/02　Lesserkai 1.4.0リリース

・フィッシャークロックルールで送られるbinc/wincに対応。

2014/04/19　Lesserkai 1.3.6リリース

・秒読みのある対局で時間内に手を指さないことがあった問題を修正。

2013/09/15　Lesserkai 1.3.5リリース

・将棋所の変更に応じて、エンジンの初期設定をファイルに保存する
　機能を削除。

2012/12/08　Lesserkai 1.3.4リリース

・コンパイラをVisual C++ 2010に変更。これにより、Windows2000では
　起動しなくなりました。
・Linuxでコンパイルするとエラーが出ることがあった問題を修正。
・ソースファイルの文字コードをUTF-8に変更。

2009/05/31　Lesserkai 1.3.3リリース

・後手の持ち駒が角１枚だけの局面から対局を開始すると、必ず先手番と
　判断していた問題を修正。
・ソースを少し整理。
・Visual C++ 2008用のプロジェクトを作成。 
・Linux用Makefile付属。

2008/07/26　Lesserkai 1.3.2リリース

・エンジン設定ダイアログで設定した値を保存していなかった問題を修正。
　（quitコマンドが送られてきた時、それ以前のコマンドを実行したのを
　確認してから終了するよう、ReceiveThread()を修正。）

2008/05/31　Lesserkai 1.3.1リリース

・持ち時間の長い対局での連続対局をすると止まってしまうことが
　あったので、最大探索深さを8から4に再変更。go infiniteで思考
　する場合（検討機能で時間無制限にした時）だけ、最大探索深さを
　8にします。

2008/03/15　Lesserkai 1.3リリース

・検討機能に対応するため、go infiniteが送られてきた時の処理を
　追加。
・最大探索深さを4から8に変更。

2008/03/03　Lesserkai修正版リリース（バージョンは1.2のままです）

・コマンド受信部分に修正を追加。

2008/03/02　Lesserkai 1.2リリース

・入玉勝ち宣言ができる局面になった場合、bestmove winを返す
　機能を追加。（入玉の判定はCSAルールに基づきます。）
・先読みをオンにして対局すると、途中で止まってしまうことが
　あった問題を修正。この修正に関連して、コマンド受信部分の
　関数（ReceiveThread()とWaitCommand()）を、以前よりも単純で
　わかりやすいものに変更しています。また、残り時間に応じて
　思考を打ち切る部分（KyokumenKomagumi::Evaluate()の最初の
　方にあるルーチン）なども修正しています。
・対局中に残り時間を大幅に超えて思考を続けることがあった
　問題を修正。（Kyokumen::CheckMate()で、詰将棋ルーチンを
　短い時間で打ち切るように変更。）

2008/02/09　Lesserkai 1.1リリース

・詰将棋解答機能を追加。
・対局時、秒読みで設定した値が反映されていなかった問題を修正。

2007/08/19　最初のリリース
